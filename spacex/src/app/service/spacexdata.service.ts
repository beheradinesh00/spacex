import { Injectable } from '@angular/core';
import {
  HttpClient,
  HttpInterceptor,
  HttpRequest,
  HttpHandler,
  HttpEvent,
} from '@angular/common/http';
import { BehaviorSubject, Observable, from } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';
//import { apiUrl } from 'src/environments/environment';
import { HttpHeaders } from '@angular/common/http';

@Injectable({ providedIn: 'root' })
export class SpacexdataService implements HttpInterceptor {
  private currentUserSubject: BehaviorSubject<any>;
  public currentUser: Observable<any>;
  private spaceXUrl = `https://api.spaceXdata.com/v3/launches`;
  private headers = new HttpHeaders()
    .set('content-type', 'application/json')
    .set('Access-Control-Allow-Origin', '*');

  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<any>(
      JSON.parse(localStorage.getItem('currentUser'))
    );
    this.currentUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue() {
    return this.currentUserSubject.value;
  }

  getSpacexData(): Observable<any> {
    let headers: HttpHeaders = new HttpHeaders();
    const url = this.spaceXUrl + '?limit=100';
    return this.http
      .get(url, { headers })
      .pipe
      // tap(_ => console.log(_)),
      ();
  }
  getSpacexDataByYear(year): Observable<any> {
    let headers: HttpHeaders = new HttpHeaders();
    const url = this.spaceXUrl + '?limit=100&launch_success=true&land_success=true&launch_year='+year;
    return this.http
      .get(url, { headers })
      .pipe
      // tap(_ => console.log(_)),
      ();
  }

  getSpacexDataByLaunchSuccess(status): Observable<any> {
    let headers: HttpHeaders = new HttpHeaders();
    const url = this.spaceXUrl + '?limit=100&launch_success=' + status;
    return this.http
      .get(url, { headers })
      .pipe
      // tap(_ => console.log(_)),
      ();
  }

  getSpacexDataByall(param): Observable<any> {
    let headers: HttpHeaders = new HttpHeaders();
    const url =
      this.spaceXUrl +
      '?limit=100launch_success=' +
      param.launch_success +
      '&land_success=' +
      param.land_success +
      '&launch_year=' +
      param.launch_year;
    return this.http
      .get(url, { headers })
      .pipe
      // tap(_ => console.log(_)),
      ();
  }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    const userToken = 'secure-user-token';
    const modifiedReq = req.clone({
      headers: req.headers.set(
        'Authorization',
        `Basic Y2tfMjI1ZDk1Y2Q1MzEwYmZlNzgzZGNiZjA5ZGNmZTBkYzkyMjVlZjU2NTpjc19jYTBiOWIyNjRiNGVhMGIzZDgwMjgyODE5ZjU5OWEwMDU3ZjQ1OGIw`
      ),
    });
    return next.handle(modifiedReq);
  }
}
