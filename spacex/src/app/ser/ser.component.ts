import { SpacexdataService } from '../service/spacexdata.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ser',
  templateUrl: './ser.component.html',
  styleUrls: ['./ser.component.css'],
})
export class SerComponent implements OnInit {
  spacexData: any;
  launchYear: any;
  launchSuccess: any;
  landSuccess: any;
  routeArray = [];
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private spacexDataService: SpacexdataService
  ) {}

  ngOnInit(): void {
    ///:launch_year/:launch_success/:land_success
    this.route.params.subscribe((params) => {
      console.log('param search result:' + JSON.stringify(params));
      if (params.type == 'all') {
        this.allDataFilter(params);
      } else if (params.type == 'bylaunchsuccess') {
        console.log('launch_success filter');
        this.launchSuccessFilter(params.value);
      } else if (params.type == 'byyear') {
        console.log('by year');
        this.byYearFilter(params.value);
      }
    });

    this.getSpacexData();
  }

  getSpacexData() {
    this.spacexDataService.getSpacexData().subscribe((data) => {
      this.spacexData = data;
      this.launchYear = data
        .map((item) => item.launch_year)
        .filter((value, index, self) => self.indexOf(value) === index);

      this.launchSuccess = data
        .map((item) => item.launch_success)
        .filter((value, index, self) => self.indexOf(value) === index);

      this.landSuccess = data
        .map((item) => item.rocket.first_stage.cores[0].land_success)
        .filter((value, index, self) => self.indexOf(value) === index);

      console.log(
        'Responce this.landSuccess:' + JSON.stringify(this.landSuccess)
      );
    });
  }

  searchByLaunchYr(year) {
    this.routeArray.push({ year: year });
    this.router.navigate(['/searchresult/byyear/' + year]);
  }

  searchByLaunchSuccess(status) {
    this.routeArray.push({ launch_success: status });
    console.log('this.routeArray: ' + JSON.stringify(this.routeArray));
    this.router.navigate(['/searchresult/bylaunchsuccess/' + status]);
  }

  allDataFilter(params) {
    this.spacexDataService.getSpacexDataByall(params).subscribe((data) => {
      this.spacexData = data;
      console.log(
        'Responce this.landSuccess:' + JSON.stringify(this.landSuccess)
      );
    });
  }

  launchSuccessFilter(value) {
    this.spacexDataService
      .getSpacexDataByLaunchSuccess(value)
      .subscribe((data) => {
        this.spacexData = data;
        console.log(
          'Responce this.landSuccess:' + JSON.stringify(this.landSuccess)
        );
      });
  }

  byYearFilter(year) {
    this.spacexDataService.getSpacexDataByYear(year).subscribe((data) => {
      this.spacexData = data;
      console.log(
        'Responce this.landSuccess:' + JSON.stringify(this.landSuccess)
      );
    });
  }
}
