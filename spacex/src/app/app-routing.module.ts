import { SerComponent } from './ser/ser.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { CommonModule } from '@angular/common';

const routes: Routes = [  {
  path: '',
  component: SerComponent,
},{
  path: 'searchresult/:launch_year/:launch_success/:land_success',
  component: SerComponent,
},{
  path: 'searchresult/:type/:value',
  component: SerComponent,
},{
  path: 'searchresult/:type/:value',
  component: SerComponent,
}];

@NgModule({
  imports: [CommonModule, RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
